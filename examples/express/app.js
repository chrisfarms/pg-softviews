var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    pg = require('pg'),
    util = require('util');

var DB_CONNECTION = {
    database: 'chrisfarms',
    host: "localhost",
    port: 5432,
    poolSize: 10
}


app.get('/', function(req, res){
    res.sendfile(__dirname + '/index.html');
});

// alice sends SELECT name FROM person WHERE id > 50;          --> registers person.name WHERE person.id > 50

// bob sends UPDATE person SET name = 'x';                     --> perons.name MATCH
//                                                             --> WHERE person.id > 50 MATCH
//                                                             --> triggers notification

// alice recieves notification of potential change to her query

// bob sends UPDATES person SET name = 'y' WHERE id < 100;     --> person.name MATCH
//                                                             --> WHERE person.id < 100 NO MATCH

// alice does NOT receive update as the condition clearly does not match

io.sockets.on('connection', function(socket){
    var user;
    socket.on('authenticate', function(data, send){
        if(!send){
            console.error('no reply function given to authenticate');
            return;
        }
        if(!data.username || !data.password){
            send({error: 'missing login details'});
            return;
        }
        if(data.username == 'x' && data.password == 'y'){
            user = {id:1, username:'x'};
            send(user);
        } else {
            send({error: 'invalid login details'});
        }
    });
    socket.on('query', function(data, send){
        query(data, user, function(err, result){
            send({
                error: err ? err.toString() : null,
                result: result
            });
        });
    });
    socket.on('disconnect', function(){
        // noop
    });
});

// define some views into the data accessable for the user
var RuleSet = require('../../index').RuleSet,
    rules = new RuleSet();


rules.use(function(rules, user){

    if(!user){
        return;
    }

    rules.view('users', {
        table: 'public.person',
        select: true,
        update: true,
        columns: {
            id:          {select: true},
            name:        {select: true, update: true}
        }
    })
})

// execute the query object in a transaction with the user's role and schema
function query(q, user, fn){
    // apply the softviews to the query
    rules.parse(q, [], user, function(err, sql, values){
        if(err){
            return fn(err);
        }
        console.log(sql)
        // excuted the compiled SQL
        pg.connect(DB_CONNECTION, function(err, db, done){
            if(err){
                return fn(err)
            }
            db.query("BEGIN; SET LOCAL SCHEMA '';", function(err){
                if(err){
                    return db.query('COMMIT', function(){
                        fn(err);
                        return done();
                    });
                }
                db.query(sql, values, function(err,result){
                    return db.query('COMMIT', function(){
                        fn(err, result);
                        return done();
                    });
                });
            });
        });
    })
}


server.listen(8080);
console.log("listening...");
