var expect = require("chai").expect;

var READ = 2,
    WRITE = 4;

describe('SQL Parser', function(){

    var parser = require('../parser');

    var schema = {
        company:   {
            id: {select:true},
            name: {select:true, update:true}
        },
        person: {
            id: {select:true, update:true},
            name: {select:true, update:true},
            company_id: {select:true, update:true},
            '*': {delete: true, insert:true}
        }
    }

    var SQL = {
        "SELECT id FROM person": {
            person: {
                id: {select:true}
            }
        },
        "SELECT id AS x FROM person": {
            person: {
                id: {select:true}
            }
        },
        "SELECT id, name FROM person": {
            person: {
                id: {select:true},
                name: {select:true}
            }
        },
        "SELECT id, name FROM person WHERE id = $1": {
            person: {
                id: {select:true},
                name: {select:true}
            }
        },
        "SELECT id, name FROM person WHERE id = $1 AND name ~* $2 OR name ~ $3": {
            person: {
                id: {select:true},
                name: {select:true}
            }
        },
        "SELECT id AS x, name AS y FROM person": {
            person: {
                id: {select:true},
                name: {select:true}
            }
        },
        "SELECT id || name AS concat FROM person": {
            person: {
                id: {select:true},
                name: {select:true}
            }
        },
        "SELECT name, (SELECT name FROM company LIMIT 1) AS company_name FROM person": {
            person: {
                name: {select:true}
            },
            company: {
                name: {select:true}
            }
        },
        "SELECT id FROM (SELECT id FROM (SELECT id FROM person WHERE id > $1) AS p1) AS p2": {
            person: {
                id: {select:true}
            }
        },
        "SELECT person.id FROM person JOIN company": {
            person: {
                id: {select:true}
            }
        },
        "SELECT person.id FROM person LEFT JOIN company": {
            person: {
                id: {select:true}
            }
        },
        "SELECT person.id FROM person LEFT OUTER JOIN company": {
            person: {
                id: {select:true}
            }
        },
        "SELECT person.id FROM person LEFT OUTER JOIN company ON person.company_id = company.id": {
            person: {
                id: {select:true},
                company_id: {select:true}
            },
            company: {
                id: {select:true}
            }
        },
        "SELECT person.id FROM person INNER JOIN company": {
            person: {
                id: {select:true}
            }
        },
        "SELECT person.id FROM person, company WHERE company_id = company.id": {
            person: {
                id: {select:true},
                company_id: {select:true}
            },
            company: {
                id: {select:true}
            }
        },
        "DELETE FROM person WHERE person.id IN (SELECT c.id FROM company AS c)": {
            company: {
                id: {select:true}
            },
            person: {
                id: {select:true},
                '*': {delete:true}
            }
        },
        "UPDATE person SET name = $1": {
            person: {
                name: {update:true}
            }
        },
        "UPDATE person SET name = $1, company_id = $2": {
            person: {
                name: {update:true},
                company_id: {update:true}
            }
        },
        "UPDATE person SET name = $1, company_id = $2 WHERE id = $3": {
            person: {
                id: {select:true},
                name: {update:true},
                company_id: {update:true}
            }
        },
        "UPDATE company SET name = $1 WHERE id IN (SELECT id FROM person)": {
            person: {
                id: {select:true}
            },
            company: {
                name:{update:true},
                id: {select:true}
            }
        },
        "INSERT INTO person ( name ) VALUES ( $1 )": {
            person: {
                '*': {insert:true},
                name: {update:true}
            }
        },
        "INSERT INTO person ( id, name ) VALUES ( $1, $2 )": {
            person: {
                id: {update:true},
                name: {update:true},
                '*': {insert:true}
            }
        },
        "SELECT id FROM user":
            parser.ValidationError,
        "SELECT pk FROM person": 
            parser.ValidationError,
        "SELECT name FROM unknown": 
            parser.ValidationError,
        "SELECT name FROM person WHERE unknown = $1": 
            parser.ValidationError,
        "SELECT name FROM person WHERE name = $1 OR unknown = $1": 
            parser.ValidationError,
        "SELECT name FROM person WHERE name = $1 AND unknown = $1": 
            parser.ValidationError,
        "SELECT unknown.name FROM person": 
            parser.ValidationError,
        "UPDATE unknown SET id = $1": 
            parser.ValidationError,
        "UPDATE person SET unknown = $1": 
            parser.ValidationError,
        "UPDATE company SET id = $1": 
            parser.ValidationError,
        "UPDATE person SET unknown = $1 WHERE id IN (SELECT id FROM unknown)": 
            parser.ValidationError,
        "INSERT INTO unknown (name) VALUES ($1)": 
            parser.ValidationError,
        "INSERT INTO company (name) VALUES ($1)": 
            parser.ValidationError,
        "INSERT INTO person (name,unknown) VALUES ($1)": 
            parser.ValidationError,
        "DELETE FROM person WHERE unknown = $1": 
            parser.ValidationError,
        "DELETE FROM unknown": 
            parser.ValidationError,
        "DELETE FROM company": 
            parser.ValidationError,
        "SELECT * FROM unknown": 
            parser.SyntaxError,
        "SELECT name FROM public.person": 
            parser.SyntaxError,
        "DELETE FROM public.unknown": 
            parser.SyntaxError,
        "UPDATE public.unknown SET name = $1": 
            parser.SyntaxError,
    }

    describe('should successfully parse and reconstruct SQL string:', function(){
        Object.keys(SQL).forEach(function(input){
            if(typeof SQL[input] != 'object'){
                return;
            }
            it(input, function(){
                var stmt = parser.parse(input, schema),
                    output = stmt.toString();
                expect(output).to.equal(input);
            })
        })
    })

    describe('should extract data sources from SQL:', function(){
        Object.keys(SQL).forEach(function(input){
            if(typeof SQL[input] != 'object'){
                return;
            }
            it(input, function(){
                var stmt = parser.parse(input, schema),
                    sources = stmt.sources;
                expect(sources).to.deep.equal(SQL[input]);
            })
        })
    })

    describe('should fail to validate SQL for:', function(){
        Object.keys(SQL).forEach(function(input){
            if(typeof SQL[input] != 'function'){
                return;
            }
            it(input, function(){
                expect(function(){
                    parser.parse(input, schema)
                }).to.throw(SQL[input]);
            })
        })
    })
});

describe('views', function(){

	var RuleSet = require('../index').RuleSet,
		rules = new RuleSet();


    rules.use(function(rules, session){

        if(session.super){
            rules.view('enterprise', {
                select: true,
                update: true,
                columns: {
                    id:          {select: true, update: true},
                    name:        {select: true, update: true}
                }
            })
        }

        rules.view('organisation', {
            select: session.admin ?
                ['enterprise_id = $1', session.enterprise_id] :
                ['id IN (SELECT organisation_id FROM organisation_member WHERE member_id = $1)', session.member_id],
            update: session.admin ?
                ['enterprise_id = $1', session.enterprise_id] :
                ['id IN (SELECT organisation_id FROM organisation_member WHERE member_id = $1)', session.member_id],
            insert: session.admin ?
                {enterprise_id: session.enterprise_id} :
                false,
            delete: session.admin ?
                ['enterprise_id = $1', session.enterprise_id] :
                false,
            columns: {
                id:            {select: true},
                name:          {select: true, update: true},
                enterprise_id: {select: true, update: session.super}
            }
        })

        rules.view('organisation_member', {
            require: ['organisation'],
            select: ['organisation_id IN (SELECT id FROM organisation)'],
            update: session.super ?
                ['organisation_id IN (SELECT id FROM organisation)'] : session.admin ?
                ['organisation_id = $1', session.organisation_id] :
                false,
            delete: session.super ?
                ['organisation_id IN (SELECT id FROM organisation)'] : session.admin ?
                ['organisation_id = $1', session.organisation_id] :
                false,
            columns: {
                organisation_id:    { select:true },
                member_id:          { select:true },
                admin:              { select:session.admin || session.super, update:session.admin || session.super}
            }
        })

        rules.view('member', {
            require: ['organisation_member'],
            select: ['id IN (SELECT member_id FROM organisation_member)'],
            update: ['id = $1', session.member_id],
            delete: ['id = $1', session.member_id],
            columns: {
                id: {select:true},
                name: {select:true, update: true},
            },
        })

        rules.view('asset', {
            select: ['organisation_id IN ?', session.organisation_id],
            columns: {
                id: {select: true},
                name: {select: true}
            }
        })
    })

    describe('should generate unique schemas for:', function(){

        it('member=true', function(){
            var views = rules.views({member:true});
            var schema = {
                organisation:           {
                    id: {select:true},
                    name: {select:true, update: true},
                    enterprise_id: {select:true}
                },
                organisation_member: {
                    organisation_id: {select:true},
                    member_id: {select:true}
                },
                member: {
                    '*': {delete:true},
                    id: {select:true},
                    name: {select:true, update:true}
                },
                asset: {
                    id: {select:true},
                    name: {select:true}
                }
            }
            expect(views.schema()).to.eql(schema);
        })

        it('admin=true', function(){
            var views = rules.views({admin:true, enterprise_id:1});
            var schema = {
                organisation:           {
                    '*': {delete:true, insert:true},
                    id: {select:true},
                    name: {select:true, update: true},
                    enterprise_id: {select:true},
                },
                organisation_member: {
                    '*': {delete:true},
                    organisation_id: {select:true},
                    member_id: {select:true},
                    admin: {select:true, update:true},
                },
                member: {
                    '*': {delete:true},
                    id: {select:true},
                    name: {select:true, update:true}
                },
                asset: {
                    id: {select:true},
                    name: {select:true}
                }
            }
            expect(views.schema()).to.eql(schema);
        })

        it('super=true', function(){
            var views = rules.views({super:true});
            var schema = {
                enterprise: {
                    id: {select:true, update:true},
                    name: {select:true, update:true}
                },
                organisation: {
                    id: {select:true},
                    name: {select:true, update: true},
                    enterprise_id: {select:true, update:true}
                },
                organisation_member: {
                    '*': {delete: true},
                    organisation_id: {select:true},
                    member_id: {select:true},
                    admin: {select:true, update:true},
                },
                member: {
                    '*': {delete:true},
                    id: {select:true},
                    name: {select:true, update:true}
                },
                asset: {
                    id: {select:true},
                    name: {select:true}
                }
            }
            expect(views.schema()).to.eql(schema);
        })

    })

	describe('should rewrite SQL for admin=true that', function(){

		var session = {
			admin: true,
			enterprise_id: 1,
			organisation_id: 2,
			member_id: 3
		};

		it('allows SELECT of all organisations belonging to the enterprise', function(done){
			rules.parse("SELECT id, name FROM organisation", [], session, function(err, sql, values){
				if(err){
					return done(err);
				}
				expect(sql).to.equal('WITH organisation AS (SELECT id, name, enterprise_id FROM organisation WHERE enterprise_id = $1) SELECT id, name FROM organisation')
				expect(values[0]).to.equal(session.enterprise_id)
				done()
			});
		})

        it('allows SELECT and filter of all organisations belonging to the enterprise', function(done){
            rules.parse("SELECT id, name FROM organisation WHERE id > $1", [10], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('WITH organisation AS (SELECT id, name, enterprise_id FROM organisation WHERE enterprise_id = $2) SELECT id, name FROM organisation WHERE id > $1')
                expect(values[0]).to.equal(10)
                expect(values[1]).to.equal(session.enterprise_id)
                done()
            });
        })

        it('allows SELECT of all membership of all organisations belong to the enterprise', function(done){
            rules.parse("SELECT id, name FROM member", [], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('WITH organisation AS (SELECT id, name, enterprise_id FROM organisation WHERE enterprise_id = $1), organisation_member AS (SELECT organisation_id, member_id, admin FROM organisation_member WHERE organisation_id IN (SELECT id FROM organisation)), member AS (SELECT id, name FROM member WHERE id IN (SELECT member_id FROM organisation_member)) SELECT id, name FROM member')
                expect(values.length).to.equal(1)
                expect(values[0]).to.equal(session.enterprise_id)
                done()
            });
        })

        it('allows UPDATE an organisation belonging to the enterprise', function(done){
            rules.parse("UPDATE organisation SET name = $1", ['bob'], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('UPDATE organisation SET name = $1 WHERE enterprise_id = $2')
                expect(values[0]).to.equal('bob')
                expect(values[1]).to.equal(session.enterprise_id)
                done()
            });
        })

        it('allows INSERT of an organisation with (automatic) relation to the enterprise', function(done){
            rules.parse("INSERT INTO organisation ( name ) VALUES ( $1 )", ['bob'], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('INSERT INTO organisation ( name, enterprise_id ) VALUES ( $1, $2 )')
                expect(values[0]).to.equal('bob')
                expect(values[1]).to.equal(session.enterprise_id)
                done()
            });
        })

        it('allows DELETE of all of my organisations', function(done){
            rules.parse("DELETE FROM organisation", [], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('DELETE FROM organisation WHERE enterprise_id = $1')
                expect(values[0]).to.equal(session.enterprise_id)
                done()
            });
        })

        it('disallows INSERT of an organisation with (manually) related to the enterprise', function(done){
            rules.parse("INSERT INTO organisation ( name, enterprise_id ) VALUES ( $1, $2 )", ['bob', session.enterprise_id], session, function(err, sql, values){
                expect(err.message).to.match(/enterprise_id/)
                done()
            });
        })

	})

    describe('should rewrite SQL for member=true that', function(){

        var session = {
            enterprise_id: 1,
            organisation_id: 2,
            member_id: 3
        };

        it('allows SELECT of all organisations that we have membership in', function(done){
            rules.parse("SELECT id, name FROM organisation", [], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('WITH organisation AS (SELECT id, name, enterprise_id FROM organisation WHERE id IN (SELECT organisation_id FROM organisation_member WHERE member_id = $1)) SELECT id, name FROM organisation')
                expect(values[0]).to.equal(session.member_id)
                done()
            });
        })

        it('allows UPDATE only of own member record', function(done){
            rules.parse("UPDATE member SET name = $1", ['bob'], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('UPDATE member SET name = $1 WHERE id = $2')
                expect(values[0]).to.equal('bob')
                expect(values[1]).to.equal(session.member_id)
                done()
            });
        })

        it('allows DELETE only of own member record', function(done){
            rules.parse("DELETE FROM member", [], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('DELETE FROM member WHERE id = $1')
                expect(values[0]).to.equal(session.member_id)
                done()
            });
        })

        it('allows DELETE only of own member record with a weird sub-select', function(done){
            rules.parse("DELETE FROM member WHERE name IN (SELECT name FROM organisation WHERE id > $1) OR true", [100], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('WITH organisation AS (SELECT id, name, enterprise_id FROM organisation WHERE id IN (SELECT organisation_id FROM organisation_member WHERE member_id = $3)) DELETE FROM member WHERE (name IN (SELECT name FROM organisation WHERE id > $1) OR true) AND (id = $2)')
                expect(values[0]).to.equal(100)
                expect(values[1]).to.equal(session.member_id)
                done()
            });
        })

        it('allows DELETE only of own member record with multiple conditions', function(done){
            rules.parse("DELETE FROM member WHERE true AND true OR false AND true OR true", [], session, function(err, sql, values){
                if(err){
                    return done(err);
                }
                expect(sql).to.equal('DELETE FROM member WHERE (true AND true OR false AND true OR true) AND (id = $1)')
                expect(values[0]).to.equal(session.member_id)
                done()
            });
        })

    })
})

