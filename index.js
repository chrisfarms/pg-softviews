var parser = require('./parser'),
    util = require('util'),
    tokens = require('./tokens')


// RuleSets define the rules, views, functions and permissions
// that will be used to generate a ViewSet for a given session
function RuleSet(){
}

// set a function that will be called to define the rules
RuleSet.prototype.use = function(fn){
    this.builder = fn;
}

// apply the rules with the session to return a ViewSet
RuleSet.prototype.views = function(session){
    return new ViewSet(this, session);
}

// convinence method for: rules.views(session).parse(sql, values, cb)
RuleSet.prototype.parse = function(sql, values, session, cb){
    return this.views(session).parse(sql, values, cb)
}

// ViewSets are instances of applied RuleSets
// They contain the generated schema for the given session
// and handle rewriting of input SQL to safe SQL for execution
// against the database
function ViewSet(rs, session){
    this.views = {};
    rs.builder.call(this, this, session);
    // add the required views
}

// output a schema from the view definitions
ViewSet.prototype.schema = function(){
    var schema = {};
    for(var name in this.views){
        schema[name] = {};
        var columns = this.views[name].columns;
         for(var c in columns){
            var o = {};
            if(columns[c].select){
                o.select = true;
            }
            if(columns[c].update){
                o.update = true;
            }
            if(columns[c].delete){
                o.delete = true;
            }
            if(columns[c].insert){
                o.insert = true;
            }
            if(Object.keys(o).length!=0){
                schema[name][c] = o;
            }
        }
    }
    return schema;
}

// define a view
ViewSet.prototype.view = function(name, def){
    this.views[name] = new View(name, def);
}

// define an allowed database function
ViewSet.prototype.function = function(name){
    this.functions[name] = true;
}

// rewrite the stmt and values
ViewSet.prototype.rewrite = function(stmt, values, cb){
    var err = null,
        sql;
    // rewrite conditions
    var skip = [];
    if(stmt instanceof tokens.Insert){
        // ensure defaults
        for(var k in this.views[stmt.source.table.name].defaults){
            stmt.columns.push(new tokens.Column(k, stmt.source.table.name));
            stmt.values.push(new tokens.Binding(values.length+1));
            values.push(this.views[stmt.source.table.name].defaults[k])
        }
        skip.push(stmt.source.table.name);
    } else if(stmt instanceof tokens.Update){
        // add general condition
        var cond = this.views[stmt.source.table.name].updateToken(values);
        if(cond){
            if(stmt.filters.where){
                cond = new tokens.Operator('AND', stmt.filters.where, cond, true);
            }
            stmt.filters.where = cond;
        }
        skip.push(stmt.source.table.name);
    } else if(stmt instanceof tokens.Delete){
        // add general condition
        var cond = this.views[stmt.source.table.name].deleteToken(values);
        if(cond){
            if(stmt.filters.where){
                cond = new tokens.Operator('AND', stmt.filters.where, cond, true);
            }
            stmt.filters.where = cond;
        }
        skip.push(stmt.source.table.name);
    }
    var require = function(name){

    }
    // add any required CTEs
    for(var name in stmt.sources){
        // don't add CTEs to tables being updated/deleted/inserted
        if(skip.indexOf(name)>=0){
            continue;
        }
        this.cte(stmt, values, this.views[name]);
    }
    // return the SQL
    cb(err, stmt.toString(), values);
}

// recursivly require all views as CTE on stmt
ViewSet.prototype.cte = function(stmt, values, v){
    for(var i=0; i<v.dependencies.length; i++){
        var req = this.views[v.dependencies[i]];
        if(!req){
            throw new Error('referenced view "' + v.dependencies[i] + '" is not defined');
        }
        this.cte(stmt, values, req);
    }
    stmt.with[v.name] = v.selectToken(values);
}

// parses the given SQL against the generated schema
// and rewrites it to conform to the rules
ViewSet.prototype.parse = function(sql, values, cb){
    try{
        var stmt = parser.parse(sql, this.schema());
        this.rewrite(stmt, values, cb);
    } catch(e) {
        return cb(e);
    }
}


// A view is a definition of a table tailored for a session
function View(name, opts){
    if(!name){
        throw new Error('expected a view name');
    }
    // the name of this view (not the table name)
    this.name = name;
    // list of column definitions
    this.columns = {};
    // conditions for statements
    this.conditions = {};
    // defaults that are forced on INSERT
    this.defaults = null;
    // other views that this view depends on
    this.dependencies = [];
    // user init
    switch(typeof opts){
        case 'function':
            throw new Error('not implimented');
            break;
        case 'object':
            for(var k in opts){
                switch(k){
                    case 'insert':
                        // log the default insert values if any
                        if(opts[k] && typeof opts[k] != 'boolean'){
                            this.defaults = opts[k];
                            opts[k] = true;
                        }
                    case 'delete':
                        // add the special * column for insert/delete
                        if(opts[k]){
                            if(!this.columns['*']){
                                this.columns['*'] = {};
                            }
                            this.columns['*'][k] = true;
                        }
                    case 'select':
                    case 'update':
                        // mark as allowed or not
                        if(opts[k]){
                            this[k] = true;
                            // log the condition if any
                            if(opts[k] && typeof opts[k] != 'boolean'){
                                this.conditions[k] = Array.isArray(opts[k]) ?
                                    opts[k] : [opts[k]];
                            }
                        } else {
                            this[k] = false;
                        }
                        break;
                    case 'columns':
                        // copy column defs
                        for(var c in opts[k]){
                            this.columns[c] = opts[k][c];
                        }
                        break;
                    case 'require':
                        for(var i=0; i<opts[k].length; i++){
                            this.dependencies.push(opts[k][i]);
                        }
                        break;
                    case 'table':
                        this.table = opts[k];
                        break;
                }
            }
            break;
        case 'undefined':
            break;
        default:
            throw new Error('expected view definition as function or object');
    }
    // if no table name given assume the view name
    if(!this.table){
        this.table = name;
    }
}

// return a list of column names that this view defines
View.prototype.columnNames = function(){
    var columns = [], c;
    for(var name in this.columns){
        c = this.columns[name];
        if(c.select || c.update){
            columns.push(name);
        }
    }
    return columns;
}

View.prototype.selectToken = function(values){
    var sql = "SELECT "+this.columnNames().join(',');
    sql += " FROM "+this.table;
    if(this.conditions.select){
        sql += " WHERE " + this.conditions.select[0];
    }
    var t = parser.tokenize(sql);
    if(this.conditions.select){
        t.offsetBindings(values.length);
        values.push.apply(values, this.conditions.select.slice(1));
    }
    return t;
}

View.prototype.updateToken = function(values){
    if(!this.update){
        return;
    }
    var t = parser.tokenize(this.conditions.update[0], 'exprs');
    t.offsetBindings(values.length);
    values.push.apply(values, this.conditions.update.slice(1));
    return t;
}

View.prototype.deleteToken = function(values){
    if(!this.delete){
        return;
    }
    var t = parser.tokenize(this.conditions.delete[0], 'exprs');
    t.offsetBindings(values.length);
    values.push.apply(values, this.conditions.delete.slice(1));
    return t;
}

exports.RuleSet = RuleSet;
