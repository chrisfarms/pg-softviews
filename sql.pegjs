{

    var tokens = require('./tokens');

    function flatten(a){
        if(Array.isArray(a)){
            return a.reduce(function(a,b){
                if(!b){
                    return a
                }
                a.push( flatten(b) );
                return a;
            },[]).join(' ');
        } else {
            return a;
        }
    }
}

start =
    stmt

stmt =
    v:(BEGIN / COMMIT / ROLLBACK / select_stmt / update_stmt / delete_stmt / insert_stmt)
    { return v }

delete_stmt =
    DELETE FROM t:table_name w:where?
    { 
        t = new tokens.Source(t);
        return new tokens.Delete(t, w ? w.where : undefined);
    }

value =
    whitespace v:(
        ( literal_value ) /
        ( bind_parameter ) /
        ( qualified_column_name ) /
        ( column_name ) /
        ( unary_operator expr ) /
        ( call_function ) /
        ( CAST lparen expr AS type_name rparen ) /
        ( CASE expr ? (WHEN expr THEN expr)+ (ELSE expr)? END ) /
        ( sub_select_expr ) /
        ( paren_expr ) /
    )
    { return v }

sub_select_expr =
    whitespace lparen v:select_stmt rparen
    { return v }

paren_expr =
    whitespace lparen v:expr whitespace rparen
    { return v }

qualified_column_name =
    t:ident "." v:column_name
    { v.qualified = t; return v }

expr =
    v:(between_expr / binary_expr / value)
    { return v }

binary_expr =
    left:value op:binary_operator right:expr
    { return new tokens.Operator(op, left, right) }

between_expr =
    v:value (NOT? BETWEEN) o1:expr AND o2:expr
    { return new tokens.BetweenOperator(v, o1, o2) }

boolean_expr = v:(
        (expr boolean_operator boolean_expr) /
        (expr boolean_operator expr)
    )
    { return new tokens.Operator(v[1], v[0], v[2]) }

binary_operator =
    whitespace v:(
        '||'
        / '*' / '/' / '%'
        / '+' / '-'
        / '<<' / '>>' / '&' / '|'
        / '<=' / '>='
        / '<' / '>'
        / '~*' / '~'
        / '=' / '==' / '!=' / '<>'
        / ('IS' 'NOT'?) / 'IN' / ('I'? 'LIKE') / 'MATCH' / 'REGEXP'
    )
    { return v }

boolean_operator =
    whitespace v:('AND' / 'OR') whitespace1
    { return v }

call_function =
    function_name
    whitespace
    lparen
    ( ( DISTINCT ? ( expr (whitespace comma expr)* )+ ) / whitespace star )?
    whitespace
    rparen
    { return 'FIXME' }

literal_value =
    v:("NULL" / "CURRENT_TIME" / "CURRENT_DATE" / "CURRENT_TIMESTAMP" / "true" / "false" / integer)
    { return new tokens.Literal(v) }

integer =
    n:[0-9]+
    { return n.join('') }

insert_stmt =
    INSERT INTO t:table_name cols:insert_columns vals:(insert_values / insert_values_from_select)
    r:(RETURNING select_exprs)?
    {
        t = new tokens.Source(t);
        return new tokens.Insert(t, cols, vals, r);
    }

insert_values =
    VALUES lparen v:expr more:(whitespace comma expr)* whitespace rparen
    { return [v].concat(more.map(function(v){ return v[2]})) }

insert_values_from_select =
    v:select_stmt
    { return v }

insert_columns =
    whitespace lparen v:column_name more:(whitespace comma column_name)* whitespace rparen
    { return [v].concat(more.map(function(v){ return v[2]}));console.log(v); }

select_stmt =
    v:(select_stmt_compound / select_stmt_single)
    { return v }

select_stmt_compound =
    left:select_stmt_single op:compound_operator right:select_stmt
    { return new tokens.Union(op, left, right) }

select_stmt_single =
    SELECT d:DISTINCT? exps:select_exprs f:from? opts:(where / group_by / having / order_by / limit / offset)*
    { return new tokens.Select(!!d, exps, f, opts) }

from =
    FROM v:source
    { return v }

where =
    WHERE v:exprs
    { return {where: v} }

exprs =
    v:(boolean_expr / expr)
    { return v }

group_by =
    GROUP BY v:ordering_term more:(whitespace comma ordering_term)*
    { return [v].concat(more.map(function(v){ return v[2]})) }

having =
    HAVING v:expr
    { return {k:'having', v:v} }

order_by =
    ORDER BY v:ordering_term more:(whitespace comma ordering_term)*
    { return [v].concat(more.map(function(v){ return v[2]})) }

limit =
    LIMIT v:expr
    { return {limit: v} }

offset =
    OFFSET v:expr
    { return {offset: v} }

select_exprs =
    v:select_expr more:(whitespace comma select_expr)*
    { return [v].concat(more.map(function(v){ return v[2]})) }

select_expr =
    whitespace v:expr al:(AS whitespace column_alias)?
    { if(al){ v.alias = al[2]}; return v }

source =
    v:(join_source / single_source)
    { return v }

join_source =
    left:single_source op:join_op right:source on:join_constraint?
    { return new tokens.Join(op, left, right, on) }

single_source_table =
    v:table_name al:(AS whitespace1 table_alias)?
    { return new tokens.Source(v, al[2]) }

single_source_select =
    lparen q:select_stmt rparen AS whitespace al:table_alias
    { return new tokens.Source(q, al) }

single_source =
    whitespace v:(single_source_table / single_source_select)
    { return v }

join_op =
    v:( join_op_comma / join_op_words)
    { return v }

join_op_comma =
    whitespace comma
    { return ',' }

join_op_words =
    s:(((LEFT OUTER?) / INNER / CROSS)? JOIN)
    { return flatten(s) }

join_constraint =
    ON v:expr
    { return v }

ordering_term =
    whitespace v:expr mod:(ASC / DESC)?
    { if(mod){v.direction = mod}; return v }

compound_operator =
    v:((UNION ALL?) / INTERSECT / EXCEPT)
    { return v.join('') }

update_stmt =
    UPDATE t:table_name SET exps:update_exprs w:where? r:(RETURNING select_exprs)?
    {
        var columns = [], values = [];
        for(var i=0; i<exps.length; i++){
            columns.push(exps[i].column);
            values.push(exps[i].value);
        }
        t = new tokens.Source(t);
        return new tokens.Update(t, columns, values, w ? w.where : undefined, r[1]) 
    }

update_exprs =
    v:update_expr more:(whitespace comma update_expr)*
    { return [v].concat(more.map(function(v){ return v[2]})) }


update_expr =
    whitespace k:column_name whitespace "=" e:expr
    { return {column: k, value:e} }

bind_parameter =
  '$' n:[0-9]+
  { return new tokens.Binding(parseInt(n.join(''),10)) }

comma =
    ","

lparen =
    "("

rparen =
    ")"

star =
    '*'

whitespace =
  [ \t\n\r]*

whitespace1 =
  [ \t\n\r]+

unary_operator =
    whitespace ( '-' / '+' / '~' / 'NOT')

ident =
  start:[A-Za-z_] rest:[A-Za-z0-9_]*
  { return start + rest.join('') }

table_name =
    q:(ident ".")? v:ident
    { return new tokens.Table(v, q[0]) }

table_alias =
    v:ident
    { return v }

column_name =
    whitespace v:ident
    { return new tokens.Column(v) }

column_alias =
    v:ident
    { return v }

function_name =
    v:ident
    { return v }

type_name =
    v:ident
    { return v }

ALL =
    whitespace1 s:"ALL"
    { return s }

AND =
    whitespace1 s:"AND"
    { return s }

AS =
    whitespace1 s:"AS"
    { return s }

ASC =
    whitespace1 s:"ASC"
    { return s }

BEGIN =
    whitespace1 s:"BEGIN"
    { return s }

BETWEEN =
    whitespace1 s:"BETWEEN"
    { return s }

BY =
    whitespace1 s:"BY"
    { return s }

CASE =
    whitespace1 s:"CASE"
    { return s }

CAST =
    whitespace1 s:"CAST"
    { return s }

COMMIT =
    whitespace1 s:"COMMIT"
    { return s }

CROSS =
    whitespace1 s:"CROSS"
    { return s }

DEFAULT =
    whitespace1 s:"DEFAULT"
    { return s }

DELETE =
    whitespace s:"DELETE"
    { return s }

DESC =
    whitespace1 s:"DESC"
    { return s }

DISTINCT =
    whitespace1 s:"DISTINCT"
    { return s }

ELSE =
    whitespace1 s:"ELSE"
    { return s }

END =
    whitespace1 s:"END"
    { return s }

EXCEPT =
    whitespace1 s:"EXCEPT"
    { return s }

EXISTS =
    whitespace1 s:"EXISTS"
    { return s }

FROM =
    whitespace1 s:"FROM" whitespace
    { return s }

GROUP =
    whitespace1 s:"GROUP"
    { return s }

HAVING =
    whitespace1 s:"HAVING"
    { return s }

IN =
    whitespace1 s:"IN"
    { return s }

INNER =
    whitespace1 s:"INNER"
    { return s }

INSERT =
    whitespace s:"INSERT"
    { return s }

INTERSECT =
    whitespace1 s:"INTERSECT"
    { return s }

INTO =
    whitespace1 s:"INTO" whitespace
    { return s }

IS =
    whitespace1 s:"IS"
    { return s }

JOIN =
    whitespace1 s:"JOIN"
    { return s }

LEFT =
    whitespace1 s:"LEFT"
    { return s }

LIKE =
    whitespace1 s:"LIKE"
    { return s }

ILIKE =
    whitespace1 s:"ILIKE"
    { return s }

LIMIT =
    whitespace1 s:"LIMIT"
    { return s }

MATCH =
    whitespace1 s:"MATCH"
    { return s }

NOT =
    whitespace1 s:"NOT"
    { return s }

OFFSET =
    whitespace1 s:"OFFSET"
    { return s }

ON =
    whitespace1 s:"ON"
    { return s }

ORDER =
    whitespace1 s:"ORDER"
    { return s }

OUTER =
    whitespace1 s:"OUTER"
    { return s }

REGEXP =
    whitespace1 s:"REGEXP"
    { return s }

ROLLBACK =
    whitespace1 s:"ROLLBACK"
    { return s }

SELECT =
    whitespace s:"SELECT"
    { return s }

SET =
    whitespace1 s:"SET" whitespace1
    { return s }

THEN =
    whitespace1 s:"THEN"
    { return s }

UNION =
    whitespace1 s:"UNION"
    { return s }

UPDATE =
    whitespace s:"UPDATE" whitespace1
    { return s }

USING =
    whitespace1 s:"USING"
    { return s }

VALUES =
    whitespace1 s:"VALUES" whitespace
    { return s }

WHEN =
    whitespace1 s:"WHEN"
    { return s }

WHERE =
    whitespace1 s:"WHERE"
    { return s }

RETURNING =
    whitespace1 s:"RETURNING"
    { return s }
