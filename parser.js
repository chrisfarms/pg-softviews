var parser = require('./sql'),
	util = require('util'),
    tokens = require('./tokens')

function arrayEqual(a, b){
    var i = Math.max(a.length, b.length, 1);
    while(i-- >= 0 && a[i] === b[i]);
    return (i === -2);
}
function flatten(arr) {
    var r = [];
    while (!arrayEqual(r, arr)){
        r = arr;
        arr = [].concat.apply([], arr);
    }
    return arr;
}

function ValidationError(message){
    ValidationError.super_.apply(this, arguments);
    this.name = 'ValidationError';
    this.message = message;
}
util.inherits(ValidationError, Error);
exports.ValidationError = ValidationError;

function ParseError(message){
    ParseError.super_.apply(this, arguments);
    this.name = 'SyntaxError';
    this.message = message;
}
util.inherits(ParseError, SyntaxError);
exports.SyntaxError = ParseError;

var REF_SOURCE = 0,
    DATA_SOURCE = 1,
    LITERAL_SOURCE = 2,
    BINDING_SOURCE = 3;
// search for a unique active table with a column by this name
function tableFor(name, scope){
    var found;
    for(var table in scope){
        for(var alias in scope[table]){
            if(alias==name){
                if(found){
                    throw new ValidationError('column reference "'+name+'" is ambiguous');
                }
                found = table;
            }
        }
    }
    return found;
}
// follow a column token and return a data source
function follow(col, scope, schema){
    var table;
    if(Array.isArray(col)){
        return flatten(col.map(function(s){
            return follow(s,scope);
        }));
    } else if(col.source){
        return [col];
    } else if(col.data){ // trust it
        return [col.data]
    } else if(col instanceof tokens.Column){
        table = col.qualified || col.table;
        if(!table){
            table = tableFor(col.name, scope);
            col.table = table;
        }
        if( !(source = table && scope[table] && scope[table][col.name]) ){
            throw new ValidationError('column "'+col.name+'" does not exist');
        }
        return follow(scope[table][col.name]).map(function(s){
            if(col.update){
                s.update = true;
            } else {
                s.select = true;
            }
            col.data = s;
            return s;
        }, scope);
    } else {
        throw new Error('attempted to follow an unexpected reference');
    }
}
// generate a flat list of columns (or literals) required to build the given expression
// these will later be resolved to sources from schema
function pseudoColumn(exp){
    if(exp instanceof tokens.Literal ){
        return [{source: LITERAL_SOURCE}];
    }else if(exp instanceof tokens.Column ){
        return [exp]; // resolve this later
    }else if(exp instanceof tokens.Binding ){
        return [{source:BINDING_SOURCE}];
    }else if(exp instanceof tokens.Join ){
        return flatten([pseudoColumn(exp.left),pseudoColumn(exp.right),pseudoColumn(exp.on)]);
    }else if(exp instanceof tokens.Operator ){
        return flatten([pseudoColumn(exp.left),pseudoColumn(exp.right)]);
    }else{
        throw new ValidationError(exp+' cannot appear in SELECT expression');
    }
}
// get all the referenced columns
// the main perpose of this method is not actually to fetch the columns
// but to validate the column references for the given stmt scope
function scopeFor(stmt, schema){
    // start with a copy of the schema scope
    var scope = {};
    // fetch all the referenced sources
    stmt.source.eachToken(function(t, depth){
        var table;
        if( t instanceof tokens.Source && depth == 0){
            if(t.query){
                if(!t.alias){
                    throw new ValidationError('subquery "' + t.toString() + '" must have an alias');
                }
                table = t.alias;
                if(scope[table]){
                    throw new ValidationError('table name "' + table +'" specified more than once');
                }
                scope[table] = {};
                t.query.expressions.forEach(function(exp){
                    var alias = exp.alias || exp.name;
                    if(!alias){
                        throw new ValidationError('expression "'+exp.toString()+'" must have an alias');
                    }
                    scope[table][alias] = pseudoColumn(exp);
                })
            } else {
                if(!schema[t.table.name]){
                    throw new ValidationError('relation "'+t.table.name+'" does not exist');
                }
                if(t.alias){
                    if(scope[t.alias]){
                        throw new ValidationError('table name: "' + t.alias + '" specified more than once');
                    }
                    table = t.alias;
                } else {
                    table = t.table.name;
                }
                scope[table] = schema[t.table.name];
            }
        }
    })
    // resolve on each source column to resolve any references
    for(var table in scope){
        for(var alias in scope[table]){
            scope[table][alias] = follow(scope[table][alias], scope);
        }
    }
    return scope;
}
// validate the statement against the given schema
// the stmt will be extended with a new "sources" property containing each
// DATA_SOURCE from the schema that is in use
function validate(stmt, schema){
    // a list of used columns
    var sources = {};
    // for each statement build a scope
    stmt.eachToken(function(t){
        if( t instanceof tokens.Statement ){
            var scope = scopeFor(t, schema)
            t.eachToken(function(t){
                if( t instanceof tokens.Column ){
                    follow(t, scope).forEach(function(s){
                        if(s.source == DATA_SOURCE){
                            if(!sources[s.table]){
                                sources[s.table] = {};
                            }
                            if(!sources[s.table][s.name]){
                                sources[s.table][s.name] = {}
                            }
                            if(s.update){
                                sources[s.table][s.name].update = s.update;
                            }
                            if(s.select){
                                sources[s.table][s.name].select = s.select;
                            }
                        }
                    });
                }
            })
            if( t instanceof tokens.Insert ){
                if(!sources[t.source.table.name]){
                    sources[t.source.table.name] = {};
                }
                sources[t.source.table.name]['*'] = {insert:true};
            } else if( t instanceof tokens.Delete ){
                if(!sources[t.source.table.name]){
                    sources[t.source.table.name] = {};
                }
                sources[t.source.table.name]['*'] = {delete:true};
            }
        } else if( t instanceof tokens.Table ){
            if(t.schema){
                throw new ParseError('schema qualified relation "' + t.toString() + '" is not allowed in this context');
            }
        }
    })
    stmt.sources = sources;
    return stmt;
}

// parse and SQL statement + validate it against a given schema object
//
// The schema object should be a simple hash of table names to column names.
//
// example:
//
// var stmt = parse("SELECT * FROM person", {
//     person: {
//          id: {select:true}
//     },
//     company:{id: {select:true}, name: {select:true}},
// });
//
// parser.SyntaxError will be thrown if there is a problem parsing the SQL 
// parser.ValidationError will be throw if the stmt has obvious inconsitancies against the schema.
//
exports.parse = function(sql, schema){
    var stmt;
    try{
        stmt = parser.parse(sql);
    } catch(err) {
        if(err instanceof parser.SyntaxError){
            var e = new ParseError(err.message);
            if(typeof err.line != 'undefined'){
                e.message += '\n' + sql.split(/\n/g)[err.line-1] + '\n';
                e.message += (new Array(err.column > 3 ? err.column -2 : err.column)).join('-') + '^^^';
            }
            if(err.stack){
                e.stack = err.stack;
            }
            throw e;
        } else {
            throw err;
        }
    }
    // copy the schema with into a source list
    var ds = {};
    for(var name in schema){
        ds[name] = {}
        for(var c in schema[name]){
            ds[name][c] = {source: DATA_SOURCE, table: name, name: c};
        }
    }
    // run the source validation
    stmt = validate(stmt, ds);
    // ensure that the sources are valid and remove weird props
    for(var name in stmt.sources){
        if(!schema[name]){
            throw new ValidationError('relation "'+name+'" does not exist');
        }
        for(var c in stmt.sources[name]){
            var o = stmt.sources[name][c];
            var s = schema[name][c];
            if(o.select && (!s || !s.select)){
                throw new ValidationError('column "'+c+'" is not selectable');
            }
            if(o.update && (!s || !s.update)){
                throw new ValidationError('column "'+c+'" is not updatable');
            }
            if(o.delete && (!s || !s.delete)){
                if(c=='*'){
                    throw new ValidationError('relation "'+name+'" is not updatable');
                } else {
                    throw new ValidationError('column "'+c+'" is not updatable');
                }
            }
            if(o.insert && (!s || !s.insert)){
                if(c=='*'){
                    throw new ValidationError('relation "'+name+'" is not insertable');
                } else {
                    throw new ValidationError('column "'+c+'" is not insertable');
                }
            }
            delete o.source;
            delete o.table;
            delete o.name;
        }
    }
    return stmt;
}

// access to the REAL parser without schema validation
exports.tokenize = function(sql, start){
    return parser.parse(sql, start) 
}
