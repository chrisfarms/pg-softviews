
var util = require('util');

function withQuery(w){
    var q = [], ws = [];
    if(w && Object.keys(w).length>0){
        q.push('WITH');
        for(var name in w){
            ws.push(name + ' AS (' + w[name].toString() + ')')
        }
        q.push(ws.join(', '));
    }
    return q;
}

function Token(){ }

// return the SQL for the token (and child tokens)
Token.prototype.toString = function(){
    throw new SyntaxError('NotImplimented');
}

// walk the whole query
Token.prototype.eachToken = function(cb, depth){
    cb(this, depth || 0);
}

Token.prototype.offsetBindings = function(offset){
    if(offset === 0){
        return;
    }
    this.eachToken(function(t){
        if( t instanceof BindingToken ){
            t.n += offset;
        }
    })
}

// ---------------------------------

function StatementToken(w){
	Token.call(this);
    this.with = w || {};
    this.filters = {};
}
util.inherits(StatementToken, Token);

StatementToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    if(this.filters){
        for(var k in this.filters){
            switch(k){
                case 'where':
                case 'having':
                case 'limit':
                case 'offset':
                    this.filters[k].eachToken(cb, depth);
                    break;
                case 'group':
                case 'order':
                    this.filters[k].forEach(function(v){
                        v.eachToken(cb, depth);
                    });
                    break;
            }
        }
    }
    if(this.returning){
        this.returning.eachToken(cb, depth);
    }
    StatementToken.super_.prototype.eachToken.call(this, cb, depth);
}

StatementToken.prototype.filterTargets = function(schema){
    var targets = {},
        merge = merger(schema);
    for(var k in this.filters){
        switch(k){
            case 'where':
            case 'having':
            case 'limit':
            case 'offset':
            case 'order':
            case 'group':
                merge(targets, this.filters[k])
                break;
        }
    }
    return targets;
}

StatementToken.prototype.filterString = function(){
    var q = [];
    // add filters
    for(var k in this.filters){
        switch(k){
            case 'where':
                q.push('WHERE');
                q.push(this.filters[k].toString())
                break;
            case 'group':
                q.push('GROUP BY');
                q.push(this.filters[k].map(function(v){
                    return v.toString();
                }).join(', '))
                break;
            case 'having':
                q.push('HAVING');
                break;
            case 'order':
                q.push('ORDER BY');
                q.push(this.filters[k].map(function(v){
                    return v.toString();
                }).join(', '))
                break;
            case 'limit':
                q.push('LIMIT');
                q.push(this.filters[k].toString())
                break;
            case 'offset':
                q.push('OFFSET');
                q.push(this.filters[k].toString())
                break;
        }
    }
    return q.join(' ');
}




// ---------------------------------

function InsertStatementToken(source, columns, values, returning){
    StatementToken.call(this);
    this.source = source;
    if(!this.source){
        throw new SyntaxError('INSERT must reference a table source not a sub-query');
    }
    this.columns = columns;
    if(!this.columns || this.columns.length===0){
        throw SyntaxError('INSERT statement must name the columns');
    }
    for(var i=0; i<this.columns.length; i++){
        this.columns[i].qualified = this.source.table.name;
        this.columns[i].update = true;
    }
    this.values = values;
    if(!this.values || this.values.length===0){
        throw SyntaxError('INSERT statement must have VALUES');
    }
    if(returning){
        this.returning = returning;
    }
}
util.inherits(InsertStatementToken, StatementToken);

InsertStatementToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    this.source.eachToken(cb, depth);
    for(var i=0; i<this.columns.length; i++){
        this.columns[i].eachToken(cb, depth);
    }
    if(Array.isArray(this.values)){
        for(var i=0; i<this.values.length; i++){
            this.values[i].eachToken(cb, depth);
        }
    } else {
        this.values.eachToken(cb, depth);
    }
    InsertStatementToken.super_.prototype.eachToken.call(this, cb, depth)
}

InsertStatementToken.prototype.toString = function(){
    var q = withQuery(this.with);
    q.push('INSERT');
    q.push('INTO');
    q.push(this.source.toString());
    if(this.columns.length>0){
        q.push('(');
        q.push(this.columns.map(function(c){
            return c.name;
        }).join(', '))
        q.push(')');
    }
    if(Array.isArray(this.values)){
        q.push('VALUES (')
        q.push( this.values.map(function(v){
            return v.toString();
        }).join(', ') )
        q.push(')');
    }else{
        q.push(this.values.toString());
    }
    if(this.returning){
        q.push('RETURNING');
        q.push( this.returning.map(function(v){
            return v.toString();
        }).join(', ') )
    }
    return q.join(' ');
}

// ---------------------------------

function UpdateStatementToken(source, columns, values, where, returning){
    StatementToken.call(this);
    this.source = source;
    if(!this.source.table){
        throw new SyntaxError('UPDATE must reference a table source not a sub-query');
    }
    this.columns = columns;
    this.values = values;
    for(var i=0; i<this.columns.length; i++){
        this.columns[i].qualified = this.source.table.name;
        this.columns[i].update = true;
    }
    if(where){
        this.filters.where = where;
    }
    if(returning){
        this.returning = returning;
    }
}
util.inherits(UpdateStatementToken, StatementToken);

UpdateStatementToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    this.source.eachToken(cb, depth);
    for(var i=0; i<this.columns.length; i++){
        this.columns[i].eachToken(cb, depth);
        this.values[i].eachToken(cb, depth);
    }
    UpdateStatementToken.super_.prototype.eachToken.call(this, cb, depth)
}

UpdateStatementToken.prototype.toString = function(){
    var q = withQuery(this.with);
    q.push('UPDATE');
    q.push(this.source.toString());
    q.push('SET');
    // add values
    var vs = [];
    for(var i=0; i<this.columns.length; i++){
        vs.push(this.columns[i].name + ' = ' + this.values[i].toString(true));
    }
    q.push(vs.join(', '));
    // add filters
    if(this.filters.where){
        q.push(this.filterString())
    }
    // add returning
    if(this.returning){
        q.push('RETURNING');
        q.push( this.returning.map(function(v){
            return v.toString();
        }).join(', ') )
    }
    return q.join(' ');
}
// ---------------------------------

function DeleteStatementToken(source, where, returning){
    StatementToken.call(this);
    this.source = source;
    if(!this.source.table){
        throw new SyntaxError('DELETE statement must referance a table name not a sub-query');
    }
    if(where){
        this.filters.where = where;
    }
    if(returning){
        this.returning = returning;
    }
}
util.inherits(DeleteStatementToken, StatementToken);

DeleteStatementToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    this.source.eachToken(cb, depth);
    DeleteStatementToken.super_.prototype.eachToken.call(this, cb, depth)
}

DeleteStatementToken.prototype.toString = function(){
    var q = withQuery(this.with);
    q.push('DELETE');
    q.push('FROM');
    q.push(this.source.toString());
    // add condition
    if(this.filters.where){
        q.push(this.filterString())
    }
    // add returning
    if(this.returning){
        q.push('RETURNING');
        q.push( this.returning.map(function(v){
            return v.toString();
        }).join(', ') )
    }
    return q.join(' ');
}

// ---------------------------------

function SelectStatementToken(distinct, expressions, source, filters){
    StatementToken.call(this);
    this.distinct = distinct;
    this.expressions = expressions;
    if(source){
        this.source = source;
    }
    if(filters){
        for(var i=0; i<filters.length; i++){
            var o = filters[i];
            for(var k in o){
                this.filters[k] = o[k];
            }
        }
    }
}
util.inherits(SelectStatementToken, StatementToken);


SelectStatementToken.prototype.eachToken = function(cb,depth){
    depth = typeof depth == 'undefined' ? 0 : depth+1;
    if(this.source){
        this.source.eachToken(cb, depth);
    }
    for(var i=0; i<this.expressions.length; i++){
        this.expressions[i].eachToken(cb, depth);
    }
    SelectStatementToken.super_.prototype.eachToken.call(this, cb, depth)
}

SelectStatementToken.prototype.toString = function(group){
    var q = withQuery(this.with);
    q.push('SELECT');
    // add DISTINCT
    if(this.distinct){
        q.push('DISTINCT');
    }
    // add SELECT columns
    q.push( this.expressions.map(function(v){
        return v.toString(true);
    }).join(', ') );
    // add source
    if(this.source){
        q.push('FROM');
        q.push( this.source.toString() );
    }
    // add filters
    if(Object.keys(this.filters).length>0){
        q.push(this.filterString());
    }
    var s = q.join(' ');
    if(group || this.alias){
        s = '(' + s + ')';
    }
    if(this.alias){
        s += ' AS '+this.alias;
    }
    return s;
}

// ---------------------------------

function ColumnToken(v, source){
	Token.call(this);
	this.name = v;
    if(source){
        this.qualified = source;
    }
}
util.inherits(ColumnToken, Token);

ColumnToken.prototype.toString = function(){
	var s = '';
    if(this.qualified){
        s += this.qualified + '.';
    }
    s += this.name;
    if(this.alias){
        s += ' AS '+ this.alias;
    }
    return s;
}

// ---------------------------------

function TableToken(v, schema){
    Token.call(this);
    this.name = v;
    if(schema){
        this.schema = schema;
    }
}
util.inherits(TableToken, Token);

TableToken.prototype.toString = function(){
    var s = '';
    if(this.schema){
        s += this.schema + '.';
    }
    s += this.name;
    return s;
}

// ---------------------------------

function LiteralToken(v){
    Token.call(this);
    this.value = v;
}
util.inherits(LiteralToken, Token);

LiteralToken.prototype.toString = function(){
    var s = this.value.toString();
    if(this.alias){
        s += ' AS '+ this.alias;
    }
    return s;
}

// ---------------------------------

function SourceToken(src, alias){
    Token.call(this);
    if(src instanceof TableToken){
        this.table = src;
    } else if(src instanceof StatementToken){
        this.query = src;
    } else {
        throw new SyntaxError('unexpected source type: '+src.constructor.name);
    }
    if(alias){
        this.alias = alias;
    }
}
util.inherits(SourceToken, Token);

SourceToken.prototype.toString = function(){
    var s = (this.table || this.query).toString();
    if(this.alias){
        s = (this.query ? '(' : '') + s + (this.query ? ')' : '') +' AS ' + this.alias;
    }
    return s;
}
SourceToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    if(this.query){
        this.query.eachToken(cb, depth);
    }
    if(this.table){
        this.table.eachToken(cb, depth);
    }
    SourceToken.super_.prototype.eachToken.call(this, cb, depth)
}

// ---------------------------------

function BindingToken(n){
	this.n = n;
	Token.call(this);
}
util.inherits(BindingToken, Token);

BindingToken.prototype.toString = function(){
	var s = "$"+this.n;
    if(this.alias){
        s += ' AS ' + this.alias;
    }
    return s;
}


// ---------------------------------

function OperatorToken(op, left, right, group){
	this.operator = op;
    this.left = left;
    this.right = right;
    this.group = group;
	Token.call(this);
}
util.inherits(OperatorToken, Token);

OperatorToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    this.right.eachToken(cb, depth);
    this.left.eachToken(cb, depth);
    OperatorToken.super_.prototype.eachToken.call(this, cb, depth);
}

OperatorToken.prototype.toString = function(){
    var left = this.left.toString(),
        right = this.right.toString(),
        s = '';
    if(this.group){
        left = '(' + left + ')';
        right = '(' + right + ')';
    }
    switch(this.operator){
        case 'IN':
            s = left + ' ' + this.operator + ' (' + right + ')';
            break;
        case ',':
            s = left + this.operator + ' ' + right;
            break;
        default:
            s = left + ' ' + this.operator + ' ' + right;
    }
    if(this.alias){
        s += ' AS ' + this.alias;
    }
    return s;
}
// ---------------------------------

function UnionOperatorToken(op, left, right, w){
    this.operator = op;
    this.left = left;
    this.right = right;
    this.with = w || {};
    UnionOperatorToken.super_.call(this, op, left, right, false);
}
util.inherits(UnionOperatorToken, OperatorToken);

UnionOperatorToken.prototype.toString = function(){
    var q = withQuery(this.with);
    q.push( UnionOperatorToken.super_.prototype.toString.call(this) );
    return q.join(' ');
}
// ---------------------------------

function JoinOperatorToken(op, left, right, on){
    this.operator = op;
    this.left = left;
    this.right = right;
    this.on = on;
    JoinOperatorToken.super_.call(this, op, left, right, false);
}
util.inherits(JoinOperatorToken, OperatorToken);

JoinOperatorToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    if(this.on){
        this.on.eachToken(cb, depth);
    }
    JoinOperatorToken.super_.prototype.eachToken.call(this, cb, depth);
}
JoinOperatorToken.prototype.toString = function(){
    var s = JoinOperatorToken.super_.prototype.toString.call(this);
    if(this.on){
        s += ' ON ' + this.on.toString();
    }
    return s;
}

// ---------------------------------

function BetweenOperatorToken(x, gt, lt){
    this.x = x;
    this.gt = gt;
    this.lt = lt;
    OperatorToken.call(this);
}
util.inherits(BetweenOperatorToken, OperatorToken);

BetweenOperatorToken.prototype.toString = function(){
    return x.toString() + ' BETWEEN ' + [gt,lt].join(' AND ');
}

BetweenOperatorToken.prototype.eachToken = function(cb, depth){
    depth = depth || 0;
    this.gt.eachToken(cb, depth);
    this.lt.eachToken(cb, depth);
    this.x.eachToken(cb, depth);
    BetweenOperatorToken.super_.prototype.eachToken.call(this, cb, depth);
}


exports.Statement = StatementToken;
exports.Select = SelectStatementToken;
exports.Insert = InsertStatementToken;
exports.Update = UpdateStatementToken;
exports.Delete = DeleteStatementToken;
exports.Binding = BindingToken;
exports.Source = SourceToken;
exports.Literal = LiteralToken;
exports.Column = ColumnToken;
exports.Table = TableToken;
exports.Operator = OperatorToken;
exports.Join = JoinOperatorToken;
exports.Union = UnionOperatorToken;
exports.BetweenOperator = BetweenOperatorToken;
